import os

import pytest
import statsd
from minty_config.store import FileStore

import minty_infrastructure


class TestInfrastructureFactoryNone:
    """Test the Infrastructure Factory with a 'none' instance-config type."""

    def test_none_config(self):
        filename = os.path.dirname(__file__) + "/data/test-none.conf"
        factory = minty_infrastructure.InfrastructureFactory(filename)

        assert factory.global_config["InstanceConfig"]["type"] == "none"
        assert factory.instance_config is None
        assert factory.statsd.connection.default_disabled

        class MockInfrastructure:
            def __init__(self, config):
                assert config["something"] == "tested"
                self.test = True

        factory.register_infrastructure(MockInfrastructure)

        infra = factory.get_infrastructure(
            infrastructure_name="MockInfrastructure", hostname="foobar.com"
        )
        assert infra.test is True


class TestInfrastructureFactory:
    """Test the Infrastructure Factory."""

    def setup_class(self):
        filename = os.path.dirname(__file__) + "/data/test.conf"
        factory = minty_infrastructure.InfrastructureFactory(filename)

        assert factory.global_config["InstanceConfig"]["type"] == "file"
        assert (
            factory.global_config["InstanceConfig"]["arguments"]["directory"]
            == "/nonexistent"
        )
        assert isinstance(factory.instance_config.store, FileStore)
        assert not factory.statsd.connection.default_disabled

        # Replace the connection with one that doesn't create network traffic
        # when running the test suite.
        statsd.Connection.set_defaults(disabled=True)
        factory.statsd.connection = statsd.Connection()

        config_dir = os.path.dirname(__file__) + "/data/test.d"
        factory.instance_config.store = FileStore(config_dir)

        self.factory = factory

    def test_basic_usage(self):
        """Test infrastructure factory initialization."""

        class MockInfrastructure:
            def __init__(self, config):
                assert config["test"] == "true"
                self.test = True

        self.factory.register_infrastructure(MockInfrastructure)

        assert "MockInfrastructure" in self.factory.registered_infrastructure

        infra = self.factory.get_infrastructure(
            infrastructure_name="MockInfrastructure", hostname="foobar.com"
        )

        assert infra.test

    def test_nonexistent_infra(self):
        """Test failure condition."""
        with pytest.raises(KeyError):
            self.factory.get_infrastructure(
                hostname="foobar.com", infrastructure_name="YYY"
            )
